from flask import Flask
from flask import Flask, render_template

app = Flask(__name__)
app.debug = True

@app.route('/', methods = ['POST', 'GET'])
def index():
  return app.send_static_file('index.html')

@app.route('/favicon.ico', methods = ['GET'])
def favicon():
  return app.send_static_file('favicon.ico')

@app.route('/static/<path:path>', methods = ['GET'])
def static_proxy(path):
  return app.send_static_file(path)

@app.route('/main', methods = ['POST', 'GET'])
def main():
  return app.send_static_file('main.html')

''' Redirect support as follows ...
@app.route('/user/<name>')
def hello_user(name):
   if name =='admin':
      return redirect(url_for('hello_admin'))
   else:
      return redirect(url_for('hello_guest',guest = name))
'''
""" Here is how to read request parameter ...
User = request.args.get(‘nm’)
"""
''' Here is parameter repass
user = request.args.get('nm')
return redirect(url_for('success',name = user))
'''
""" Static file from template referencing
<script type = "text/javascript"
         src = "{{ url_for('static', filename = 'hello.js') }}" ></script>
"""
if __name__ == '__main__':
    app.run()
