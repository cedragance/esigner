var bundle_sr_RS = {
	appName : "ATM REST интерфејс aпп УИ",
	
	city : "Град",
	
	distance : "Раздаљина",
	
	houseNumber : "Кућни број",
	
	lattitude : "Географска ширина",
	load : "Учитај",
	longitude : "Географска дужина",
	
	noMessages : "Тренутно нема порука.",
	
	postalCode : "Поштански број",
	
	save : "Сачувај",
	search : "Нађи",
	searchATMs : "Претражи АТМ-ове",
	street : "Улица",
	
	title : "Шта можемо да урадимо са овим банкоматима данас?",
	type : "Тип"
};