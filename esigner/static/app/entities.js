
var ATMView = function(atm) {
	var atmView = {};
	atmView.address_city = atm.address.city;
	atmView.address_street = atm.address.street;
	atmView.address_postalcode = atm.address.postalcode;
	atmView.address_housenumber = atm.address.housenumber;
	atmView.address_geoLocation_lat = atm.address.geoLocation.lat;
	atmView.address_geoLocation_lng = atm.address.geoLocation.lng;
	atmView.distance = atm.distance;
	atmView.type = atm.type;
	return atmView;
}

var ATMsViewTable = function(atms) {
	var result = [];
	for(var i=0; i<atms.length; i++) {
		var ATM = ATMView(atms[i]);
		result.push(ATM);
	}
	return result;
}

var ATM = function(atmview) {
	
}