# Javascript 'ESigning' prototype to avoid NPAPI limitations.

Programmed in 
### NetBeans 8.2 Patch 2, Python 2.7.14, nodejs v9.5.0, Flask 0.12.2, Webix UI v.5.1.0, SQLAlchemy 1.1.16

Dev. install steps
### pip install Flask
###

Run with
### FLASK_APP=esigner.routes.py flask run
### python esigner.routes.py
### ./app_runner.sh
### ... or similar ... depending on routes.py current state

